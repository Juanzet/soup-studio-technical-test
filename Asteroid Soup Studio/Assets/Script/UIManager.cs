using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    [SerializeField] Slider[] slider;
    [SerializeField] int index;
    
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        AssingSliderIndex(index);
    }

    public void AssingSliderIndex(int i)
    {
        slider[i].onValueChanged.AddListener(val => SoundManager.instance.ChangeMasterVolume(val));
    }


}
