using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour, IDamagable
{
    Rigidbody2D rb;
   int damage = 1;
   int health = 3;
   int force = 15;

    void Awake() 
   {
     rb = GetComponent<Rigidbody2D>();
   }

    void Start() 
    {
        RandomImpulse();
    }

    void Update() 
    {
        transform.Rotate(0,0,60 * Time.deltaTime);
        //Debug.Log(health);
       // TakeDamage(damage);
    }
   public void TakeDamage(int dmg)
    {    
        health -= dmg;
        if(health<=0) 
        {

            GameManager.instance.AddPoints(5);
            //SoundManager.instance.Play("Destroy");
            Destroy(this.gameObject);
        }
         
    }

    void RandomImpulse()
    {
        int force = Random.Range(0,4);
        switch (force)
        {
            case 0:
            rb.AddForce(Vector2.up * force);
            break;

            case 1:
            rb.AddForce(Vector2.down * force);
            break;

            case 2:
            rb.AddForce(Vector2.right * force);
            break;

            case 3:
            rb.AddForce(Vector2.left * force);
            break;
            
            default:
            break;
        }
    }

   private void OnCollisionEnter2D(Collision2D other) 
   {
        if(other.gameObject.CompareTag("Starship"))
        {
            other.gameObject.GetComponent<IDamagable>().TakeDamage(damage);
        } 
   }
}
