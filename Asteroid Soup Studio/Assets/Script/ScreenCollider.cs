using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCollider : MonoBehaviour
{
    [SerializeField] Transform opositeSide;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        GameManager.instance.SwapPosition(opositeSide, other.gameObject.transform);
        
    }
}
