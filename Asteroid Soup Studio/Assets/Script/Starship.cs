using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class Starship : MonoBehaviour,IDamagable
{
    #region  starshipParameters
    Rigidbody2D rb;
    Vector2 xy;
    //Vector2 rot;
    [SerializeField] float speed;
    [SerializeField] float force;
    [SerializeField] Transform bulletPosition;
    #endregion

    int health = 3;
    //int rotationSpeed = 15;

    void Awake() 
    {
        rb = GetComponent<Rigidbody2D>();
        
    }
    void Update() 
    {
        if(health <= 0)
        {
            GameManager.instance.PauseGame();
            SoundManager.instance.Stop("Background");
            SoundManager.instance.Play("Destroy");
        }        
    }
    void FixedUpdate()
    {
        MoveLogic();
    }
    void MoveLogic()
    {   
       rb.AddForce(xy * speed);

       // rb.AddForce(new Vector2(0,xy.y) * speed);
        //transform.rotation = Quaternion.Lerp(transform.rotation,Quaternion.Euler(0,0,xy.x * rotationSpeed), rotationSpeed); 
 
    }
    public void InstanceBullet()
    {
       GameObject bullet = GameManager.instance.GetPooledObject();

       if(bullet != null)
       {
           // SoundManager.instance.Play("Shot");
            bullet.transform.position = bulletPosition.position;
            bullet.SetActive(true);
            bullet.GetComponent<Rigidbody2D>().AddForce(this.transform.up * force);
            
       }
    }
    public void Move(InputAction.CallbackContext context)
    {
        xy = context.ReadValue<Vector2>();

    }

    // public void Rotation(InputAction.CallbackContext context)
    //{
    //   rot = context.ReadValue<Vector2>();
    //}
    //
    public void TakeDamage(int dmg)
    {
        health -= dmg;
    }
}
