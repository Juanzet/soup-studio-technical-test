using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{   
    #region Bullet poolling
    List<GameObject> pooledObjects = new List<GameObject>();
    public static GameManager instance;
    int amountToPool = 20;
    [SerializeField] GameObject bullet;
    #endregion

    #region High Score & Score
    [SerializeField]TMP_Text tScore;
    [SerializeField] TMP_Text hScore;
    int Score;
    int HighScore;
    #endregion
    
    #region Asteroid Pooling
        List<GameObject> a_pooledObjects = new List<GameObject>();
        [SerializeField] Transform[] tList;
        int asteroidAmount = 7;
        int dificult = 1;
        [SerializeField] float timer = 1f;
        [SerializeField] GameObject asteroid;
    #endregion

    #region Timer
        float timeElapsed;
        int minutes,seconds,cents;
        public TMP_Text timerText;

    #endregion

   #region Pause Menu
    InputAction prees;
    [SerializeField] GameObject pauseMenu;
   #endregion 
   
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    void Start() 
    { 
        StartCoroutine("InstantiateAsteroid");
        //SoundManager.instance.Play("Background");
        AddObjectToList();
        //AddAsteroidToList();
    }

    void Update() 
    {
        ScoreLogic();
        ClockLogic();  
        Debug.Log(timer);  
    }

    public void AddPoints(int points)
    {
        Score += points;
    }

    void ScoreLogic()
    {
        tScore.text = $"Score: {Score}";
    }

    #region Pause Menu
    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Play()
    {
        SceneManager.LoadScene("Game");
    }
    public void Options()
    {
        SceneManager.LoadScene("Options");
    }


    public void QuitGame()
    {
        Application.Quit();
    }
    #endregion
    
    #region Bullet Pooling
    void AddObjectToList()
    {
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(bullet);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if(!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }
        return null;
    }
    #endregion

    #region Asteroid Pooling
    void InstantiateAsteroidsRng()
    {
        for (int i = 0; i < asteroidAmount + dificult; i++)
        {
            int rng = Random.Range(0,4);
            GameObject obj = Instantiate(asteroid, tList[rng]);
            Debug.Log(i);
        }
    }

   /* public GameObject GetPooledAsteroid()
    {
        for (int i = 0; i < a_pooledObjects.Count; i++)
        {
            if(!a_pooledObjects[i].activeInHierarchy)
            {
                return a_pooledObjects[i];
            }
        }
        return null;
    }
    */


    IEnumerator InstantiateAsteroid()
    {
        InstantiateAsteroidsRng();
        yield return new WaitForSeconds(timer);
    }

    #endregion

    #region ClockTimer
    void ClockLogic()
    {
        
        dificult = seconds;
        timeElapsed += Time.deltaTime;

        minutes = (int)(timeElapsed / 60f);
        seconds = (int)(timeElapsed - minutes * 60f);
        cents = (int)((timeElapsed - (int)timeElapsed) * 100f);

        timerText.text = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, cents);
    }

    #endregion

    #region AsteroidScreenEfect
    public void SwapPosition(Transform pos, Transform obj)
    {
        obj.position = pos.position;
    }
    #endregion
}
