using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody2D rb;
    int damage = 1;
    float speed = 5;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
      //rb.AddForce(Vector2.up * speed);  
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.CompareTag("Asteroid")) 
        {
            other.gameObject.GetComponent<IDamagable>().TakeDamage(damage);
            gameObject.SetActive(false);
        }
    
    }


}
